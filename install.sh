#!/bin/bash


if [ ! -d "~/.vim/bundle/neobundle.vim/" ]; then
    git clone https://github.com/Shougo/neobundle.vim.git ~/.vim/bundle/neobundle.vim
    git clone https://github.com/Shougo/vimproc.vim.git ~/.vim/bundle/vimproc.vim
    cd ~/.vim/bundle/vimproc.vim
    make
fi

rm -rf ~/.vimrc && cat ~/.vim/.vimrc > ~/.vimrc
