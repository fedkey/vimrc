## vim自动化插件安装配置


### 注意
    请自行备份 ~/.vimrc配置文件 和 ~/.vim文件夹,该配置在安装时会覆盖掉原 ~/.vimrc

### 依赖
    sudo apt-get install cscope ctags python-dev python-pip (debian系)
    sudo pip install pep8 pyflakes jedi tox pytest

### Linux安装
    sudo apt-get install vim-gtk exuberant-ctags [ 其他非 Debian/Ubuntu 系的 Linux 请使用系统对应的包管理器进行安装 ]
    删除个人主目录下的 .vim 文件夹和 .vimrc 文件（如果存在的话） [ 命令为 rm -rf ~/.vim ~/.vimrc ]

    1.执行：git clone https://git.oschina.net/fedkey/vimrc.git ~/.vim
    2. sh ~/.vim/install.sh
    3. 启动 vim 或 gvim执行： NeoBundleInstall

